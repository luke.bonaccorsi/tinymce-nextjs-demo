import { Editor } from "../components/tinymce/editor";
import { documentToHtmlString } from "@contentful/rich-text-html-renderer";
import styles from "../styles/Home.module.css";

export default function Home({ body }) {
  return (
    <main className={styles.wrapper}>
      <h1>TinyMCE Next.js Demo</h1>
      <form action="/api/form" method="post" className={styles.form}>
        <label htmlFor="title" className={styles.form__label}>
          Title:
        </label>{" "}
        <input name="title" id="title" className={styles.form__input} />
        <label htmlFor="name" className={styles.form__label}>
          Name:
        </label>{" "}
        <input name="name" id="name" className={styles.form__input} />
        <Editor
          body={documentToHtmlString(body)}
          name="body"
          className={styles.form__editor}
        />
        <button className={styles.form__submit} type="submit">
          Submit
        </button>
      </form>
    </main>
  );
}
export async function getServerSideProps(context) {
  // Here a call to contentful could be made to retrieve the content
  return {
    props: {
      body: {
        nodeType: "document",
        data: {},
        content: [
          {
            nodeType: "heading-1",
            content: [
              { nodeType: "text", value: "Hello world", marks: [], data: {} },
            ],
            data: {},
          },
          {
            nodeType: "paragraph",
            content: [
              {
                nodeType: "text",
                value:
                  "This is some content provided to the editor from getServerSideProps",
                marks: [],
                data: {},
              },
            ],
            data: {},
          },
        ],
      },
    },
  };
}
