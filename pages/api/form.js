import TurndownService from "turndown";
import { richTextFromMarkdown } from "@contentful/rich-text-from-markdown";

const turndownService = new TurndownService();

export default async function handler(req, res) {
  // Get data submitted in request's body.
  const body = req.body;

  const markdown = turndownService.turndown(body.body);
  const richText = await richTextFromMarkdown(markdown);

  body.body = richText;

  // Found the name.
  // Sends a HTTP success code
  res.status(200).json(body);
}
