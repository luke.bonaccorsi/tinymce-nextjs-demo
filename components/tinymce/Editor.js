import { Editor as TinyMCE } from "@tinymce/tinymce-react";
import { useState, useRef } from "react";

export const Editor = ({ body, name, className }) => {
  const [content, setContent] = useState(body);
  const editorRef = useRef(null);

  const EditorConfig = {
    menubar: false,
    plugins: ["lists"],
    browser_spellcheck: true,
    language: "en",
    language_url: "/tinymce/langs/en.js",
    paste_data_images: false,
    force_p_newlines: false,
    branding: false,
    toolbar:
      "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist",
  };

  return (
    <div className={className}>
      <TinyMCE
        id="Editor"
        tinymceScriptSrc="/tinymce/tinymce.min.js"
        value={content}
        init={EditorConfig}
        onEditorChange={setContent}
        onInit={(evt, editor) => (editorRef.current = editor)}
        onDirty={() => editorRef.current.save()} //this updates the textarea so the form can be submitted without JS
        textareaName={name}
      />
    </div>
  );
};
